.. _example:

Examples
=========

Define a magic signature
-----------------------------
.. literalinclude:: example/cortex.yml
    :language: yaml
