===================
Data Request Broker
===================
---------------------------------
MAGIC signature for DRB
--------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-signature-magic/month
    :target: https://pepy.tech/project/drb-signature-magic
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-signature-magic.svg
    :target: https://pypi.org/project/drb-signature-magic/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-signature-magic.svg
    :target: https://pypi.org/project/drb-signature-magic/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-signature-magic.svg
    :target: https://pypi.org/project/drb-signature-magic/
    :alt: Python Version Support Badge

-------------------

This module implements magic signature to define node on DRB data model.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

