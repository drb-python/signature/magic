.. _install:

Installation of magic signature
====================================
To include this module into your project, the `drb-signature-magic` module shall be referenced into requirements.txt file,
or the following pip line can be run:

.. code-block::

    pip install drb-signature-magic
