from drb.exceptions.core import DrbException


class DrbSignatureMagicException(DrbException):
    pass
