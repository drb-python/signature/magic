from pathlib import Path
from drb.drivers.file import DrbFileFactory
from drb.exceptions.core import DrbException
from drb.signatures.magic import MagicSignature
import io
import os
import unittest
import drb.topics.resolver as resolver


class TestDrbMagicSignature(unittest.TestCase):
    mock_pkg = None
    current_path = Path(os.path.dirname(os.path.realpath(__file__)))
    empty_file = current_path / "files" / "empty.file"

    @classmethod
    def setUpClass(cls) -> None:
        current_path = Path(os.path.dirname(os.path.realpath(__file__)))
        cls.test_file_bytes = current_path / "files" / "test_bytes.magic"
        cls.test_file_txt = current_path / "files" / "test_text.magic"
        cls.test_file_regex = current_path / "files" / "test_regex.magic"
        cls.test_file_las = current_path / "files" / "test_LAS.magic"
        cls.test_file_pkzip = current_path / "files" / "test_pkzip.magic"

    def test_signature_hexa(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_bytes)

        arg = {'type': 'hexa', 'offset': 10, 'pattern': "f2 f3 f4"}

        signature_test = MagicSignature(arg)
        stream_io = node_file.get_impl(io.BufferedIOBase)
        self.assertTrue(signature_test.check_signature(stream_io))

    def test_signature_pkzip_bytes(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_pkzip)

        arg = {'type': 'bytes', 'offset': 0, 'pattern': '\x50\x4b\x03\x04'}

        signature_test = MagicSignature(arg)
        stream_io = node_file.get_impl(io.BufferedIOBase)
        self.assertTrue(signature_test.check_signature(stream_io))

        arg = {'type': 'bytes', 'offset': 0, 'pattern': b'PK\x03\x04'}

        signature_test = MagicSignature(arg)
        stream_io = node_file.get_impl(io.BufferedIOBase)
        self.assertTrue(signature_test.check_signature(stream_io))

    def test_signature_pkzip_hexa(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_pkzip)

        arg = {'type': 'hexa', 'offset': 0, 'pattern': '50 4b 03 04'}

        signature_test = MagicSignature(arg)
        stream_io = node_file.get_impl(io.BufferedIOBase)
        self.assertTrue(signature_test.check_signature(stream_io))

    def test_signature_txt(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_txt)

        arg = {}
        arg['type'] = 'string'
        arg['offset'] = 10
        arg['pattern'] = 'ACDC'

        signature_test = MagicSignature(arg)
        stream_io = node_file.get_impl(io.BufferedIOBase)
        self.assertTrue(signature_test.check_signature(stream_io))

    def test_signature_txt_in_bytes(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_txt)

        arg = {}
        arg['type'] = 'string'
        arg['offset'] = 10
        arg['pattern'] = b'\x41\x43\x44\x43'

        signature_test = MagicSignature(arg)
        stream_io = node_file.get_impl(io.BufferedIOBase)
        self.assertTrue(signature_test.check_signature(stream_io))

    def test_signature_regex(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_regex)

        arg = {}
        arg['type'] = 'regex'
        arg['offset'] = 20
        arg['pattern'] = '\\d* type_file=\\w\\w\\w\\s'

        stream_io = node_file.get_impl(io.BufferedIOBase)
        signature_test = MagicSignature(arg)

        arg['pattern'] = '\\d* type_file=\\w\\w\\w\\s\\s'

        self.assertTrue(signature_test.check_signature(stream_io))
        signature_test = MagicSignature(arg)
        self.assertFalse(signature_test.check_signature(stream_io))

    def test_signature_to_dict(self):
        arg = {'type': 'regex', 'offset': 20,
               'pattern': '\\d* type_file=\\w\\w\\w\\s'}

        signature_test = MagicSignature(arg)

        dict_sign = signature_test.to_dict()
        self.assertEqual(dict_sign['pattern'], '\\d* type_file=\\w\\w\\w\\s')
        self.assertEqual(dict_sign['offset'], 20)
        self.assertEqual(dict_sign['type'], 'regex')

    def test_signature_without_pattern(self):
        arg = {'type': 'string'}

        with self.assertRaises(DrbException):
            MagicSignature(arg)

    def test_signature_entry_point(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_bytes)

        signature, base_node = resolver.resolve(node_file)
        self.assertEqual('Test signature magic', signature.label)

    def test_signature_entry_point_LAS(self):
        node_file = DrbFileFactory().create(
            TestDrbMagicSignature.test_file_las)

        signature, base_node = resolver.resolve(node_file)
        self.assertEqual('Test signature magic LAS', signature.label)
